const vueDirectivesMap = {
    'v-文本' : 'v-text',
    // 'v-html' : 'v-html',
    'v-显示' : 'v-show',
    'v-如果' : 'v-if',
    'v-或如' : 'v-else-if',
    'v-否则' : 'v-else',
    'v-遍历' : 'v-for',
    'v-事件' : 'v-on',
    'v-绑定' : 'v-bind',
    'v-双绑' : 'v-model',
    'v-插槽' : 'v-slot',
    'v-原样' : 'v-pre',
    'v-遮盖' : 'v-cloak',
    'v-一次' : 'v-once'
}

const vonModifiersMap = {
    阻止: 'stop',
    禁止: 'prevent',
    捕捉: 'capture',
    自身: 'self',
    自身: 'self',
    // keycode | keyalias
    原生: 'native',
    一次: 'once',
    左键: 'left',
    右键: 'right',
    中键: 'middle',
    被动: 'passive',
    精确: 'exact',
    回车: 'enter',
    删除: 'delete', // 删除 和 退格 键
    空格: 'space',
    上箭头: 'up',
    下箭头: 'down',
    左箭头: 'left',
    右箭头: 'right',
    向上翻页: 'page-up',
    向下翻页: 'page-down',
    // tab, ese, ctrl, alt, shift, f*, ...
}

const vbindModifiersMap = {
    DOM属性: 'prop',
    驼峰: 'camel',
    同步: 'sync'
}

const comOptsMap = {
    名称: 'name',
    组件集: 'components',
    数据: 'data',
    属性集: 'props',
    属性数据: 'propsData',
    计算: 'computed',
    方法集: 'methods',
    监视: 'watch',
    元素: 'el',
    模板: 'template',
    渲染: 'render',
    渲染出错: 'renderError',
    创建前: 'beforeCreate',
    创建后: 'created',
    销毁前: 'beforeDestroy',
    销毁后: 'destroyed',
    挂载前: 'beforeMount',
    挂载后: 'mounted',
    更新前: 'beforeUpdate',
    更新后: 'updated',
    激活后: 'activated',
    失活后: 'deactivated',
    错误捕获后: 'errorCaptured',
    指令集: 'directives',
    过渡集: 'transitions',
    过滤器集: 'filters',
    提供: 'provide',
    注入: 'inject',
    双绑: 'model',
    父实例: 'parent',
    混入集: 'mixins',
    派生集: 'extends',
    分界符集: 'delimiters',
    是否注释: 'comments',
    是否继承属性: 'inheritAttrs'
}

module.exports = {
    vonModifiersMap,
    vbindModifiersMap,
    comOptsMap
}
