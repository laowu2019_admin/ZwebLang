const t = require("@babel/types");
const trv = require("@babel/traverse");
// import traverse from "@babel/traverse";
// import * as t from "@babel/types";
const { globalIdentMap , mathMap , consoleMap , documentMap , JSONMap, onEventMap } = require("./zjsdata");
const { tagMap } = require("./zhtmdata");
const traverse = trv.default;

function astHandler(ast) {
    traverse(ast, {
        enter(path) {
            if (t.isCallExpression(path.node)) {
                // console.log(path.node);
                if (path.node.callee.name === "类型") { // 类型(o) -> typeof o
                    path.replaceWith(t.unaryExpression('typeof', path.node.arguments[0]));
                }/*  else
                if (path.node.callee.name === "是实例") { // 是实例(a, b) -> a instanceof b
                    path.replaceWith(t.binaryExpression('instanceof', path.node.arguments[0], path.node.arguments[1]));
                }  else
                if (path.node.callee.property?.name === "创建元素" || path.node.callee.property?.name === "查询选择器") {
                    // console.log(path.node);
                    let elementName = path.node.arguments[0].value;
                    if (elementName in tagMap) {
                        if (typeof tagMap[elementName] === "string") {
                            path.node.arguments[0].value = tagMap[elementName];
                        } else {
                            path.node.arguments[0].value = tagMap[elementName].eName;
                        }
                    }
                } else 
                if (path.node.callee.property?.name === "添加事件监听" || path.node.callee.property?.name === "addEventListener") {
                    // console.log(path.node);
                    let eventName = path.node.arguments[0].value;
                    let eventNameWithOn = eventName + '时';
                    if (eventNameWithOn in onEventMap) {
                        path.node.arguments[0].value = onEventMap[eventNameWithOn].slice(2);
                    }
                }*/
            }

            /* if (t.isMemberExpression(path.node)) {
                // console.log(path.node);
                switch (path.node.object.name) {
                    case "控制台":
                        path.node.property.name = consoleMap[path.node.property.name] || path.node.property.name;
                        break;
                    
                    case "文档":
                        path.node.property.name = documentMap[path.node.property.name] || path.node.property.name;
                        break;
                    
                    case "数学":
                        path.node.property.name = mathMap[path.node.property.name] || path.node.property.name;
                        break;
                
                    case "JSON":
                        path.node.property.name = JSONMap[path.node.property.name] || path.node.property.name;
                        break;
                
                    default:
                        break;
                }
            } */

            /* if (t.isIdentifier(path.node)) { //toString 非常奇怪
                // console.log(path.node);
                if (path.node.name.match(/[\u4e00-\u9fa5]/) && path.node.name in globalIdentMap) { // hasOwnProperty in globalIdentMap
                    // console.log(path.node.name, typeof path.node.name);
                    path.node.name = globalIdentMap[path.node.name];
                } else
                if (path.node.name.slice(-1) === "时"  && path.node.name in onEventMap) {
                    path.node.name = onEventMap[path.node.name];
                }
            } */
        }
    });
}

module.exports = {
    astHandler
}