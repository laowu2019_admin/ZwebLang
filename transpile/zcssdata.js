const atruleMap = {
    字符集: 'charset',
    计数器样式: 'counter-style',
    字体名称: 'font-face',
    字体特性值: 'font-feature-values',
    导入: 'import',
    关键帧: 'keyframes',
    媒体: 'media',
    名称空间: 'namespace',
    页面: 'page',
    支持: 'supports'
}

const mediaQueryMap = { // 待补充
    且: 'and',
    非: 'not',
    仅: 'only',
    全部: 'all',
    打印: 'print',
    屏幕: 'screen',
    宽度: 'width',
    最小宽度: 'min-width',
    最大宽度: 'max-width',
    最小分辨率: 'min-resolution',
    最大分辨率: 'max-resolution',
    方向: 'orientation',
    纵向: 'portrait',
    横向: 'landscape',
}

const pseudoElementMap = {
    之后: 'after',
    之前: 'before',
    背景: 'backdrop',
    信号: 'cue',
    信号区域: 'cue-region',
    第一个字母: 'first-letter',
    第一行: 'first-line',
    文件选择器按钮: 'file-selector-button',
    选定内容: 'selection',
    标记: 'marker',
    占位符: 'placeholder',
    槽中元素: 'slotted'
}

const pseudoClassMap = {
    之后: 'after',  // 同时也是伪元素
    之前: 'before',
    第一个字母: 'first-letter',
    第一行: 'first-line',
    方向: 'dir', // 语言相关
    语言: 'lang',
    任意链接: 'any-link', // 位置相关
    未访问: 'link',
    已访问: 'visited',
    本地链接: 'local-link',
    目标: 'target',
    // '目标-其内': 'target-within', // 实验属性, 暂不列入
    作用域: 'scope',
    悬停: 'hover', // 用户操作
    激活: 'active',
    获得焦点: 'focus',
    '获得焦点-可见': 'focus-visible',
    '获得焦点-后代': 'focus-within',
    当前: 'current', // 时间相关
    过去: 'past',
    未来: 'future',
    播放: 'playing', // 资源状态
    暂停: 'paused',
    // 自动填充: 'autofill', // 输入
    启用: 'enabled',
    禁用: 'disabled',
    只读: 'read-only',
    读写: 'read-write',
    显示占位符: 'placeholder-shown',
    默认: 'default',
    选中: 'checked',
    未定: 'indeterminate',
    空白: 'blank',
    有效: 'valid',
    无效: 'invalid',
    范围内: 'in-range',
    范围外: 'out-of-range',
    必填: 'required',
    可选: 'optional',
    '用户-无效': 'user-invalid',
    '根': 'root', // 树结构
    '空': 'empty',
    第n个子元素: 'nth-child',
    倒数第n个子元素: 'nth-last-child',
    首个子元素: 'first-child',
    最后子元素: 'last-child',
    唯一子元素: 'only-child',
    类型第n个: 'nth-of-type',
    类型倒数第n个: 'nth-last-of-type',
    类型第一个: 'first-of-type',
    类型最后一个: 'last-of-type',
    类型仅一个: 'only-of-type',
    已定义: 'defined', // 其他
    第一页: 'first',
    全屏: 'fullscreen',
    含有: 'has',
    是: 'is',
    左页: 'left',
    非: 'not',
    右页: 'right',
    其中: 'where'
}

const alignValueMap = {
    开始: 'start',
    结束: 'end',
    弹性盒开始: 'flex-start',
    弹性盒结束: 'flex-end',
    居中: 'center',
    // 正常: 'normal',
    基线: 'baseline',
    第一基线: 'first baseline',
    最后基线: 'last baseline',
    均匀分布: 'space-between', // 两侧贴边, 项目之间的间隔相等
    留白分布: 'space-around', // 两侧留白, 项目之间间隔比项目与边框的间隔大一倍
    留白均匀分布: 'space-evenly', // 两侧留白, 项目之间间隔与项目与边框的间隔相等
    拉伸: 'stretch',
    安全: 'safe',
    不安全: 'unsafe'
}

const colorMap = { // https://baike.baidu.com/item/WEB%E6%A0%87%E5%87%86%E9%A2%9C%E8%89%B2
    当前颜色: 'currentcolor',
    透明: 'transparent',
    爱丽丝蓝: 'aliceblue',
    古董白: 'antiquewhite',
    湖绿: 'aqua',
    碧绿: 'aquamarine',
    青白色: 'azure',
    米色: 'beige',
    陶坯黄: 'bisque',
    黑色: 'black',
    杏仁白: 'blanchedalmond',
    蓝色: 'blue',
    蓝紫色: 'blueviolet',
    褐色: 'brown',
    硬木褐: 'burlywood',
    军服蓝: 'cadetblue',
    查特酒绿: 'chartreuse',
    巧克力色: 'chocolate',
    珊瑚红: 'coral',
    矢车菊蓝: 'cornflowerblue',
    玉米穗黄: 'cornsilk',
    绯红: 'crimson',
    青色: 'cyan',
    深蓝: 'darkblue',
    深青: 'darkcyan',
    深金菊黄: 'darkgoldenrod',
    暗灰: 'darkgray',
    深绿: 'darkgreen',
    深卡其色: 'darkkhaki',
    深品红: 'darkmagenta',
    深橄榄绿: 'darkolivegreen',
    深橙: 'darkorange',
    深洋兰紫: 'darkorchid',
    深红: 'darkred',
    深鲑红: 'darksalmon',
    深海藻绿: 'darkseagreen',
    深岩蓝: 'darkslateblue',
    深岩灰: 'darkslategray',
    深松石绿: 'darkturquoise',
    深紫: 'darkviolet',
    深粉: 'deeppink',
    深天蓝: 'deepskyblue',
    昏灰: 'dimgray',
    道奇蓝: 'dodgerblue',
    火砖红: 'firebrick',
    花卉白: 'floralwhite',
    森林绿: 'forestgreen',
    紫红: 'fuchsia',
    庚氏灰: 'gainsboro',
    幽灵白: 'ghostwhite',
    金色: 'gold',
    金菊黄: 'goldenrod',
    灰色: 'gray',
    调和绿: 'green',
    黄绿色: 'greenyellow',
    蜜瓜绿: 'honeydew',
    艳粉: 'hotpink',
    印度红: 'indianred',
    靛蓝: 'indigo',
    象牙白: 'ivory',
    卡其色: 'khaki',
    薰衣草紫: 'lavender',
    薰衣草红: 'lavenderblush',
    草坪绿: 'lawngreen',
    柠檬绸黄: 'lemonchiffon',
    浅蓝: 'lightblue',
    浅珊瑚红: 'lightcoral',
    浅青: 'lightcyan',
    浅金菊黄: 'lightgoldenrodyellow',
    亮灰: 'lightgray',
    浅绿: 'lightgreen',
    浅粉: 'lightpink',
    浅鲑红: 'lightsalmon',
    浅海藻绿: 'lightseagreen',
    浅天蓝: 'lightskyblue',
    浅岩灰: 'lightslategray',
    浅钢青: 'lightsteelblue',
    浅黄: 'lightyellow',
    绿色: 'lime',
    青柠绿: 'limegreen',
    亚麻色: 'linen',
    洋红: 'magenta',
    栗色: 'maroon',
    中碧绿: 'mediumaquamarine',
    中蓝: 'mediumblue',
    中洋兰紫: 'mediumorchid',
    中紫: 'mediumpurple',
    中海藻绿: 'mediumseagreen',
    中岩蓝: 'mediumslateblue',
    中嫩绿: 'mediumspringgreen',
    中松石绿: 'mediumturquoise',
    中紫红: 'mediumvioletred',
    午夜蓝: 'midnightblue',
    薄荷乳白: 'mintcream',
    雾玫瑰红: 'mistyrose',
    鹿皮色: 'moccasin',
    土著白: 'navajowhite',
    藏青: 'navy',
    旧蕾丝白: 'oldlace',
    橄榄色: 'olive',
    橄榄绿: 'olivedrab',
    橙色: 'orange',
    橘红: 'orangered',
    洋兰紫: 'orchid',
    白金菊黄: 'palegoldenrod',
    白绿色: 'palegreen',
    白松石绿: 'paleturquoise',
    白紫红: 'palevioletred',
    番木瓜橙: 'papayawhip',
    粉扑桃色: 'peachpuff',
    秘鲁红: 'peru',
    粉色: 'pink',
    李紫: 'plum',
    粉末蓝: 'powderblue',
    紫色: 'purple',
    红色: 'red',
    瑞贝卡紫: 'rebeccapurple',
    玫瑰褐: 'rosybrown',
    品蓝: 'royalblue',
    鞍褐: 'saddlebrown',
    鲑红: 'salmon',
    沙褐: 'sandybrown',
    海藻绿: 'seagreen',
    贝壳白: 'seashell',
    土黄赭: 'sienna',
    银色: 'silver',
    天蓝: 'skyblue',
    岩蓝: 'slateblue',
    岩灰: 'slategray',
    雪白: 'snow',
    春绿: 'springgreen',
    钢青: 'steelblue',
    日晒褐: 'tan',
    鸭翅绿: 'teal',
    蓟紫: 'thistle',
    番茄红: 'tomato',
    松石绿: 'turquoise',
    紫罗兰色: 'violet',
    麦色: 'wheat',
    白色: 'white',
    烟雾白: 'whitesmoke',
    黄色: 'yellow',
    暗黄绿色: 'yellowgreen'
}

const sizeMap = {
    最大内容: 'max-content',
    最小内容: 'min-content',
    适合内容: 'fit-content'
}

const borderStyleMap = {
    隐藏: 'hidden',
    点状: 'dotted',
    虚线: 'dashed',
    实线: 'solid',
    双线: 'double',
    槽状: 'groove',
    脊状: 'ridge',
    凹陷: 'inset',
    凸起: 'outset'
}

const borderWidthMap = {
    细: 'thin',
    中等: 'medium',
    粗: 'thick'
}

const gradDirMap = {
    至: 'to',
    上: 'top',
    下: 'bottom',
    左: 'left',
    右: 'right',
}

const imageSrcMap = {
    图像: 'image',
    图像集: 'image-set',
    元素: 'element',
    绘画: 'paint',
    淡入淡出: 'cross-fade',
    线性渐变: {
        eName: 'linear-gradient',
        paras: {
            ...colorMap,
            ...gradDirMap
        }
    },
    重复线性渐变: {
        eName: 'repeating-linear-gradient',
        paras: {
            ...colorMap,
            ...gradDirMap
        }
    },
    径向渐变: {
        eName: 'radial-gradient',
        paras: {
            ...colorMap,
            ...gradDirMap
        }
    },
    重复径向渐变: {
        eName: 'repeating-radial-gradient',
        paras: {
            ...colorMap,
            ...gradDirMap
        }
    },
    锥形渐变: {
        eName: 'conic-gradient',
        paras: {
            ...colorMap,
            ...gradDirMap
        }
    }
}

const timeFuncMap = {
    慢快慢: 'ease',
    慢速开始: 'ease-in',
    慢速结束: 'ease-out',
    慢速开始结束: 'ease-in-out',
    线性: 'linear',
    贝塞尔曲线: 'cubic-bezier',
    步进: {
        eName: 'steps',
        paras: {
            '跳跃-开始': 'jump-start',
            '跳跃-结束': 'jump-end',
            '跳跃-无': 'jump-none',
            '跳跃-启停': 'jump-both',
            开始: 'start',
            结束: 'end'
        }
    },
    步进开始: 'step-start',
    步进结束: 'step-end'
}

const filterMap = {
    模糊: 'blur',
    亮度: 'brightness',
    对比度: 'contrast',
    阴影: 'drop-shadow',
    灰阶: 'grayscale',
    色调旋转: 'hue-rotate',
    反转: 'invert',
    不透明度: 'opacity',
    饱和度: 'saturate',
    深褐色: 'sepia'
}

const gridRCMap = {
    ...sizeMap,
    最小最大: 'minmax',
    重复: {
        eName: 'repeat',
        paras: {
            最小最大: 'minmax'
        }
    },
    子栅格: 'subgrid'
}

const breakMap = {
    避免: 'avoid',
    避免分页: 'avoid-page',
    避免分列: 'avoid-column',
    避免分区: 'avoid-region',
    分页: 'page',
    分列: 'column',
    分区: 'region',
    始终: 'always',
    全部: 'all',
    左: 'left',
    右: 'right',
    正面: 'recto',
    反面: 'verso'
}

const clipShapeMap = {
    圆形: 'circle',
    椭圆: 'ellipse',
    多边形: 'polygon',
    矩形: 'inset',
    路径: 'path'
}

const propertyMap = {
    垂直方向对齐内容: {
        eName: 'align-content',
        values: alignValueMap
    },
    垂直方向对齐元素: { //对齐项目?
        eName: 'align-items',
        values: alignValueMap
    },
    垂直方向对齐自身: {
        eName: 'align-self',
        values: alignValueMap
    },
    垂直方向对齐轨道: {
        eName: 'align-tracks',
        values: alignValueMap
    },
    水平方向对齐轨道: {
        eName: 'justify-tracks',
        values: alignValueMap
    },
    全部: {
        eName: 'all',
        values: {
            还原: 'revert',
        }
    },
    动画延时: 'animation-delay',
    动画方向: {
        eName: 'animation-direction',
        values: {
            // 正常: 'normal',
            反向: 'reverse',
            交替: 'alternate',
            交替反向: 'alternate-reverse'
        }
    },
    动画持续时间: 'animation-duration',
    动画填充模式: { // 定义动画播放时间之外的状态
        eName: 'animation-fill-mode',
        values: {
            最后画面: 'forwards',
            开始画面: 'backwards',
            两者兼用: 'both'
        }
    },
    动画重复次数: {
        eName: 'animation-iteration-count',
        values: {
            无限: 'infinite'
        }
    },
    动画名称: 'animation-name',
    动画播放状态: {
        eName: 'animation-play-state',
        values: {
            暂停: 'paused',
            播放: 'running'
        }
    },
    动画时序函数: {
        eName: 'animation-timing-function',
        values: timeFuncMap
    },
    外观: {
        eName: 'appearance',
        values: {
            菜单列表按钮: 'menulist-button',
            文本字段: 'textfield',
        }
    },
    纵横比: 'aspect-ratio',
    背景滤镜: {
        eName: 'backdrop-filter',
        values: filterMap
    },
    背面可见性: {
        eName: 'backface-visibility',
        values: {
            可见: 'visible',
            隐藏: 'hidden'
        }
    },
    '背景附着': {
        eName: 'background-attachment',
        values: {
            固定: 'fixed',
            局部: 'local',
            滚动: 'scroll'
        }
    },
    '背景混合模式': { // https://blog.csdn.net/dwb123456123456/article/details/84563367
        eName: 'background-blend-mode',
        values: {
            // 正常: 'normal',
            正片叠底: 'multiply',
            滤色: 'screen',
            叠加: 'overlay',
            变暗: 'darken',
            变亮: 'lighten',
            颜色减淡: 'color-dodge',
            颜色加深: 'color-burn',
            强光: 'hard-light',
            柔光: 'soft-light',
            差值: 'difference',
            排除: 'exclusion',
            色调: 'hue',
            饱和度: 'saturation',
            颜色: 'clor',
            光度: 'luminosity'
        }
    },
    '背景裁剪': {
        eName: 'background-clip',
        values: {
            边框盒: 'border-box',
            内间距盒: 'padding-box',
            内容盒: 'content-box',
            文本: 'text'
        }
    },
    '背景颜色': {
        eName: 'background-color',
        values: colorMap
    },
    '背景图像': {
        eName: 'background-image',
        values: imageSrcMap
    },
    '背景原点': {
        eName: 'background-origin',
        values: {
            边框盒: 'border-box',
            内间距盒: 'padding-box',
            内容盒: 'content-box'
        }
    },
    '背景位置': {
        eName: 'background-position',
        values: {
            左: 'left',
            右: 'right',
            居中: 'center',
            上: 'top',
            下: 'bottom'
        }
    },
    '背景水平位置': {
        eName: 'background-position-x',
        values: {
            左: 'left',
            右: 'right',
            居中: 'center'
        }
    },
    '背景垂直位置': {
        eName: 'background-position-y',
        values: {
            居中: 'center',
            上: 'top',
            下: 'bottom'
        }
    },
    '背景平铺': {
        eName: 'background-repeat',
        values: {
            水平平铺: 'repeat-x',
            垂直平铺: 'repeat-y',
            平铺: 'repeat',
            留白: 'space',
            铺满: 'round',
            不平铺: 'no-repeat'
        }
    },
    '背景大小': {
        eName: 'background-size',
        values: {
            包含: 'contain',
            覆盖: 'cover'
        }
    },
    块大小: {
        eName: 'block-size',
        values: sizeMap
    },
    高度: {
        eName: 'height',
        values: sizeMap
    },
    宽度: {
        eName: 'width',
        values: sizeMap
    },
    最大高度: {
        eName: 'max-height',
        values: sizeMap
    },
    最大宽度: {
        eName: 'max-width',
        values: sizeMap
    },
    最小高度: {
        eName: 'min-height',
        values: sizeMap
    },
    最小宽度: {
        eName: 'min-width',
        values: sizeMap
    },
    '边框块颜色': {
        eName: 'border-block-color',
        values: colorMap
    },
    '边框块样式': {
        eName: 'border-block-style',
        values: borderStyleMap
    },
    '边框块宽度': {
        eName: 'border-block-width',
        values: borderWidthMap
    },
    '边框块结束颜色': {
        eName: 'border-block-end-color',
        values: colorMap
    },
    '边框块结束样式': {
        eName: 'border-block-end-style',
        values: borderStyleMap
    },
    '边框块结束宽度': {
        eName: 'border-block-end-width',
        values: borderWidthMap
    },
    '边框块开始颜色': {
        eName: 'border-block-start-color',
        values: colorMap
    },
    '边框块开始样式': {
        eName: 'border-block-start-style',
        values: borderStyleMap
    },
    '边框块开始宽度': {
        eName: 'border-block-start-width',
        values: borderWidthMap
    },
    '边框-下-颜色': {
        eName: 'border-bottom-color',
        values: colorMap
    },
    '边框-下-样式': {
        eName: 'border-bottom-style',
        values: borderStyleMap
    },
    '边框-下-宽度': {
        eName: 'border-bottom-width',
        values: borderWidthMap
    },
    '边框左下半径': 'border-bottom-left-radius',
    '边框右下半径': 'border-bottom-right-radius',
    '边框-上-颜色': {
        eName: 'border-top-color',
        values: colorMap
    },
    '边框-上-样式': {
        eName: 'border-top-style',
        values: borderStyleMap
    },
    '边框-上-宽度': {
        eName: 'border-top-width',
        values: borderWidthMap
    },
    '边框左上半径': 'border-top-left-radius',
    '边框右上半径': 'border-top-right-radius',
    '边框-左-颜色': {
        eName: 'border-left-color',
        values: colorMap
    },
    '边框-左-样式': {
        eName: 'border-left-style',
        values: borderStyleMap
    },
    '边框-左-宽度': {
        eName: 'border-left-width',
        values: borderWidthMap
    },
    '边框-右-颜色': {
        eName: 'border-right-color',
        values: colorMap
    },
    '边框-右-样式': {
        eName: 'border-right-style',
        values: borderStyleMap
    },
    '边框-右-宽度': {
        eName: 'border-right-width',
        values: borderWidthMap
    },
    '边框合并': {
        eName: 'border-collapse',
        values: {
            合并: 'collapse',
            分开: 'separate'
        }
    },
    '边框半径': 'border-radius',
    '边框-结束-结束-半径': 'border-end-end-radius',
    '边框-结束-开始-半径': 'border-end-start-radius',
    '边框-开始-结束-半径': 'border-start-end-radius',
    '边框-开始-开始-半径': 'border-start-start-radius',
    '边框图像超出量': 'border-image-outset',
    '边框图像平铺': {
        eName: 'border-image-repeat',
        values: {
            拉伸: 'stretch',
            铺满: 'round',
            平铺: 'repeat',
            留白: 'space'
        }
    },
    '边框图像内移量': {
        eName: 'border-image-slice',
        values: {
            填充: 'fill'
        }
    },
    '边框图像来源': {
        eName: 'border-image-source',
        values: imageSrcMap
    },
    '边框图像宽度': 'border-image-width',
    '边框行内颜色': {
        eName: 'border-inline-color',
        values: colorMap
    },
    '边框行内样式': {
        eName: 'border-inline-style',
        values: borderStyleMap
    },
    '边框行内宽度': {
        eName: 'border-inline-width',
        values: borderWidthMap
    },
    '边框行内结束颜色': {
        eName: 'border-inline-end-color',
        values: colorMap
    },
    '边框行内结束样式': {
        eName: 'border-inline-end-style',
        values: borderStyleMap
    },
    '边框行内结束宽度': {
        eName: 'border-inline-end-width',
        values: borderWidthMap
    },
    '边框行内开始颜色': {
        eName: 'border-inline-start-color',
        values: colorMap
    },
    '边框行内开始样式': {
        eName: 'border-inline-start-style',
        values: borderStyleMap
    },
    '边框行内开始宽度': {
        eName: 'border-inline-start-width',
        values: borderWidthMap
    },
    '边框间距': 'border-spacing',
    下: 'bottom',
    上: 'top',
    左: 'left',
    右: 'right',
    '盒子装饰-断开': {
        eName: 'box-decoration-break',
        values: {
            分割: 'slice',
            克隆: 'clone',
        }
    },
    '盒子阴影': {
        eName: 'box-shadow',
        values: {
            内部: 'inset',
            ...colorMap
        }
    },
    '盒子大小': {
        eName: 'box-sizing',
        values: {
            内容盒: 'content-box',
            边框盒: 'border-box'
        }
    },
    其后断开: {
        eName: 'break-after',
        values: {
            ...breakMap
        }
    },
    其前断开: {
        eName: 'break-before',
        values: {
            ...breakMap
        }
    },
    内部断开: {
        eName: 'break-inside',
        values: {
            避免: 'avoid',
            避免分页: 'avoid-page',
            避免分列: 'avoid-column',
            避免分区: 'avoid-region'
        }
    },
    其后分页: {
        eName: 'page-break-after',
        values: {
            避免: 'avoid',
            始终: 'always',
            左: 'left',
            右: 'right',
            正面: 'recto',
            反面: 'verso'
        }
    },
    其前分页: {
        eName: 'page-break-before',
        values: {
            避免: 'avoid',
            始终: 'always',
            左: 'left',
            右: 'right',
            正面: 'recto',
            反面: 'verso'
        }
    },
    内部分页: {
        eName: 'page-break-inside',
        values: {
            避免: 'avoid',
        }
    },
    表格标题位置: {
        eName: 'caption-side',
        values: {
            下: 'bottom',
            上: 'top',
            块开始: 'block-start',
            块结束: 'block-end',
            行内开始: 'inline-start',
            行内结束: 'inline-end'
        }
    },
    文本输入光标颜色: {
        eName: 'caret-color',
        values: {
            ...colorMap
        }
    },
    颜色: {
        eName: 'color',
        values: colorMap
    },
    清除: {
        eName: 'clear',
        values: {
            左: 'left',
            右: 'right',
            左右: 'both',
            行内开始: 'inline-start',
            行内结束: 'inline-end'
        }
    },
    裁剪路径: { // TODO: 参数中的 at
        eName: 'clip-path',
        values: clipShapeMap
    },
    列数: 'column-count',
    列间隙: 'column-gap',
    行间隙: 'row-gap',
    '列规则-颜色': {
        eName: 'column-rule-color',
        values: colorMap
    },
    '列规则-样式': {
        eName: 'column-rule-style',
        values: borderStyleMap
    },
    '列规则-宽度': {
        eName: 'column-rule-width',
        values: borderWidthMap
    },
    // columns 即 列数 + 列宽, 舍去
    列宽: 'column-width',
    跨列: {
        eName: 'column-span',
        values: {
            全部: 'all'
        }
    },
    限制: {
        eName: 'contain',
        values: {
            严格: 'strict',
            内容: 'content',
            大小: 'size',
            布局: 'layout',
            样式: 'style',
            绘制: 'paint',
        }
    },
    内容: {
        eName: 'content',
        values:{
            属性: 'attr',
            计数器: 'counter'
        }
    },
    // 属性: 'attr', // 应作为函数放在 content 下面
    内容可见性: {
        eName: 'content-visibility',
        values:{
            可见: 'visible',
            // 自动: 'auto',
            隐藏: 'hidden'
        }
    },
    计数器递增: 'counter-increment',
    计数器重置: 'counter-reset',
    计数器设置: 'counter-set',
    方向: {
        eName: 'direction',
        values: {
            左至右: 'ltr',
            右至左: 'rtl'
        }
    },
    浮动: {
        eName: 'float',
        values: {
            左: 'left',
            右: 'right',
            行内开始: 'inline-start',
            行内结束: 'inline-end'
        }
    },
    显示: { // https://blog.csdn.net/VhWfR2u02Q/article/details/79081137
        eName: 'display',
        values: {
            块: 'block',
            行内: 'inline',
            行内块: 'inline-block',
            行内弹性盒: 'inline-flex',
            行内表格: 'inline-table',
            行内栅格: 'inline-grid',
            流: 'flow',
            流根: 'flow-root',
            表格: 'table',
            弹性盒: 'flex',
            栅格: 'grid',
            列表项: 'list-item',
            表体: 'table-row-group',
            表头: 'table-header-group',
            表尾: 'table-footer-group',
            表行: 'table-row',
            表单元格: 'table-cell',
            表列组: 'table-column-group',
            表列: 'table-column',
            表格标题: 'table-caption'
        }
    },
    字体大小: {
        eName: 'font-size',
        values: {
            超超小: 'xx-small',
            超小: 'x-small',
            小: 'small',
            中等: 'medium',
            大: 'large',
            超大: 'x-large',
            超超大: 'xx-large',
            超超超大: 'xxx-large',
            更大: 'larger',
            更小: 'smaller'
        }
    },
    字体样式: {
        eName: 'font-style',
        values: {
            // 正常: 'normal',
            斜体: 'italic',
            倾斜: 'oblique'
        }
    },
    字体粗细: {
        eName: 'font-weight',
        values: {
            // 正常: 'normal',
            粗体: 'bold',
            更粗: 'bolder',
            更细: 'lighter'
        }
    },
    字体族: 'font-family',
    字体特性设置: {
        eName: 'font-feature-settings',
        values: {
            // 正常: 'normal',
            开: 'on',
            关: 'off'
        }
    },
    字体字距: 'font-kerning',
    字体语言覆盖: 'font-language-override',
    字体大小调整: 'font-size-adjust',
    字体伸缩: {
        eName: 'font-stretch',
        values: {
            // 正常: 'normal',
            半紧缩: 'semi-condensed',
            紧缩: 'condensed',
            超紧缩: 'extra-condensed',
            极致紧缩: 'ultra-condensed',
            半伸展: 'semi-expanded',
            伸展: 'expanded',
            超伸展: 'extra-expanded',
            极致伸展: 'ultra-expanded'
        }
    },
    '字体变体-东亚': {
        eName: 'font-variant-east-asian',
        values: {
            // 正常: 'normal',
            全宽: 'full-width',
            比例宽度: 'proportional-width',
            注音: 'ruby',
            简体: 'simplified',
            繁体: 'traditional'
        }
    },
    行高: 'line-height',
    位置: {
        eName: 'position',
        values: {
            静态: 'static',
            绝对: 'absolute',
            相对: 'relative',
            固定: 'fixed',
            粘性: 'sticky',
        }
    },
    引号: 'quotes',
    可见性: {
        eName: 'visibility',
        values: {
            可见: 'visible',
            隐藏: 'hidden',
            坍缩: 'collapse'
        }
    },
    溢出: {
        eName: 'overflow',
        values: {
            可见: 'visible',
            隐藏: 'hidden',
            裁剪: 'clip',
            滚动: 'scroll'
        }
    },
    溢出换行: { // word-wrap 为别名
        eName: 'overflow-wrap',
        values: {
            断字: 'break-word',
            任意: 'anywhere'
        }
    },
    水平溢出: {
        eName: 'overflow-x',
        values: {
            可见: 'visible',
            隐藏: 'hidden',
            裁剪: 'clip',
            滚动: 'scroll'
        }
    },
    垂直溢出: {
        eName: 'overflow-y',
        values: {
            可见: 'visible',
            隐藏: 'hidden',
            裁剪: 'clip',
            滚动: 'scroll'
        }
    },
    透视: 'perspective',
    透视原点: {
        eName: 'perspective-origin',
        values: {
            左: 'left',
            右: 'right',
            居中: 'center',
            上: 'top',
            下: 'bottom'
        }
    },
    指针事件: 'pointer-events',
    文本溢出: {
        eName: 'text-overflow',
        values: {
            裁剪: 'clip',
            省略号: 'ellipsis',
            渐变: 'fade'
        }
    },
    '列表样式-图像': {
        eName: 'list-style-image',
        values: imageSrcMap
    },
    '列表样式-位置': {
        eName: 'list-style-positon',
        values: {
            内部: 'inside',
            外部: 'outside',
        }
    },
    '列表样式-类型': {
        eName: 'list-style-type',
        values: {
            圆点: 'disc',
            圆圈: 'circle',
            方块: 'square',
            数字: 'decimal',
            '数字-前置0': 'decimal-leading-zero',
            小写罗马: 'lower-roman',
            大写罗马: 'upper-roman',
            小写希腊: 'lower-greek',
            小写字母: 'lower-alpha',
            大写字母: 'upper-alpha',
            小写中文: 'simp-chinese-informal',
            大写中文: 'simp-chinese-formal'
        }
    },
    内间距: 'padding',
    '内间距-上': 'padding-top',
    '内间距-右': 'padding-right',
    '内间距-下': 'padding-bottom',
    '内间距-左': 'padding-left',
    外间距: 'margin',
    '外间距-上': 'margin-top',
    '外间距-右': 'margin-right',
    '外间距-下': 'margin-bottom',
    '外间距-左': 'margin-left',
    文本对齐: {
        eName: 'text-align',
        values: {
            左: 'left',
            右: 'right',
            开始: 'start',
            结束: 'end',
            居中: 'center',
            两端齐整: 'justify' // 对多行有效
        }
    },
    '文本对齐-最后一行': {
        eName: 'text-align-last',
        values: {
            左: 'left',
            右: 'right',
            开始: 'start',
            结束: 'end',
            居中: 'center',
            两端齐整: 'justify' // 对单行有效
        }
    },
    文本缩进: 'text-indent',
    '文本修饰-线': {
        eName: 'text-decoration-line',
        values: {
            下划线: 'underline',
            上划线: 'overline',
            删除线: 'line-through'
        }
    },
    '文本修饰-颜色': {
        eName: 'text-decoration-color',
        values: colorMap
    },
    '文本修饰-样式': {
        eName: 'text-decoration-style',
        values: {
            实线: 'solid',
            双线: 'double',
            点状: 'dotted',
            虚线: 'dashed',
            波浪: 'wavy'
        }
    },
    '文本修饰-粗细': {
        eName: 'text-decoration-thickness',
        values: {
            来自字体: 'from-font'
        }
    },
    文本齐整: {
        eName: 'text-justify',
        values: {
            字间: 'inter-word',
            字符间: 'inter-character'
        }
    },
    文本阴影: {
        eName: 'text-shadow',
        values: colorMap
    },
    文本变换: {
        eName: 'text-transform',
        values: {
            首字母大写: 'capitalize',
            全大写: 'uppercase',
            全小写: 'lowercase',
            全宽: 'full-width'
        }
    },
    垂直对齐: {
        eName: 'vertical-align',
        values: {
            基线: 'baseline',
            上标: 'super',
            下标: 'sub',
            文本上方: 'text-top',
            文本下方: 'text-bottom',
            中间: 'middle',
            上: 'top',
            下: 'bottom'
        }
    },
    '过渡延时': 'transition-delay',
    '过渡持续时间': 'transition-duration',
    '过渡属性': {
        eName: 'transition-property',
        values: { // 这个属性可以是所有 css 属性? 需翻译中文属性名称
            全部: 'all'
        }
    },
    '过渡时序函数': {
        eName: 'transition-timing-function',
        values: timeFuncMap
    },
    表格布局: {
        eName: 'table-layout',
        values: {
            固定: 'fixed'
        }
    },
    空单元格: {
        eName: 'empty-cells',
        values: {
            隐藏: 'hide',
            显示: 'show'
        }
    },
    标题位置: { // 题注?
        eName: 'caption-side',
        values: {
            上: 'top',
            下: 'bottom',
            块开始: 'block-start',
            块结束: 'block-end',
            行内开始: 'inline-end',
            行内结束: 'inline-end'
        }
    },
    调整大小: {
        eName: 'resize',
        values: {
            双向: 'both',
            水平: 'horizontal',
            垂直: 'vertical'
        }
    },
    轮廓颜色: {
        eName: 'outline-color',
        values: {
            反转: 'invert',
            ...colorMap
        }
    },
    轮廓样式: {
        eName: 'outline-style',
        values: borderStyleMap
    },
    轮廓宽度: {
        eName: 'outline-width',
        values: borderWidthMap
    },
    轮廓偏移: 'outline-offset',
    不透明度: 'opacity',
    变换: {
        eName: 'transform',
        values: {
            矩阵: 'matrix',
            矩阵3d: 'matrix3d',
            透视: 'perspective',
            旋转: 'rotate',
            旋转3d: 'rotate3d',
            旋转X: 'rotateX',
            旋转Y: 'rotateY',
            旋转Z: 'rotateZ',
            缩放: 'scale',
            缩放3d: 'scale3d',
            缩放X: 'scaleX',
            缩放Y: 'scaleY',
            缩放Z: 'scaleZ',
            偏斜: 'skew',
            偏斜X: 'skewX',
            偏斜Y: 'skewY',
            平移: 'translate',
            平移3d: 'translate3d',
            平移X: 'translateX',
            平移Y: 'translateY',
            平移Z: 'translateZ'
        }
    },
    变换原点: {
        eName: 'transform-origin',
        values: {
            左: 'left',
            右: 'right',
            居中: 'center',
            上: 'top',
            下: 'bottom'
        }
    },
    变换样式: {
        eName: 'transform-style',
        values: {
            平面: 'flat',
            保留3d: 'preserve-3d'  // 开启3d?
        }
    },
    平移: 'translate',
    光标: {
        eName: 'cursor',
        values: {
            默认: 'default',
            小手: 'pointer',
            移动: 'move',
            文本: 'text',
            禁止: 'not-allowed',
            右键菜单: 'context-menu',
            帮助: 'help',
            进行中: 'progress',
            等待: 'wait',
            单元格: 'cell',
            十字线: 'crosshair',
            竖排文本: 'vertical-text',
            快捷方式: 'alias',
            复制: 'copy',
            可抓取: 'grab',
            抓住: 'grabbing',
            全向滚动: 'all-scroll',
            调整列: 'col-resize',
            调整行: 'row-resize',
            放大: 'zoom-in',
            缩小: 'zoom-out',
            左右箭头: 'ew-resize',
            上下箭头: 'ns-resize',
            右上箭头: 'nesw-resize',
            左上箭头: 'nwse-resize' // 还有一些暂不支持的箭头
        }
    },
    空格: {
        eName: 'white-space',
        values: {
            // 正常: 'normal',
            '合并-不换行': 'nowrap',
            保留: 'pre',
            '保留-换行': 'pre-wrap',
            '合并-换行': 'pre-line',
            断开空格: 'break-spaces'
        }
    },
    滤镜: {
        eName: 'filter',
        values: filterMap
    },
    弹性盒放大比例: 'flex-grow',
    弹性盒缩小比例: 'flex-shrink',
    弹性盒基准: {
        eName: 'flex-basis',
        values: {
            内容: 'content',
            填充: 'fill',
            ...sizeMap
        }
    },
    弹性盒方向: {
        eName: 'flex-direction',
        values: {
            行: 'row',
            '行-反向': 'row-reverse',
            列: 'column',
            '列-反向': 'column-reverse'
        }
    },
    弹性盒换行: {
        eName: 'flex-wrap',
        values: {
            换行: 'wrap',
            不换行: 'nowrap',
            '换行-反向': 'wrap-reverse',
        }
    },
    水平方向对齐内容: {
        eName: 'justify-content',
        values: {
            左: 'left',
            右: 'right',
            ...alignValueMap
        }
    },
    放置内容: {
        eName: 'place-content',
        values: {
            左: 'left',
            右: 'right',
            ...alignValueMap
        }
    },
    水平方向对齐元素: {
        eName: 'justify-items',
        values: {
            左: 'left',
            右: 'right',
            自身开始: 'slef-start',
            自身结束: 'slef-end',
            ...alignValueMap
        }
    },
    放置元素: {
        eName: 'place-items',
        values: {
            左: 'left',
            右: 'right',
            ...alignValueMap
        }
    },
    水平方向对齐自身: {
        eName: 'justify-self',
        values: {
            左: 'left',
            右: 'right',
            自身开始: 'slef-start',
            自身结束: 'slef-end',
            ...alignValueMap
        }
    },
    放置自身: {
        eName: 'place-self',
        values: {
            左: 'left',
            右: 'right',
            ...alignValueMap
        }
    },
    顺序: 'order',
    '栅格模板区域': 'grid-template-areas',
    '栅格模板列': {
        eName: 'grid-template-columns',
        values: gridRCMap
    },
    '栅格模板行': {
        eName: 'grid-template-rows',
        values: gridRCMap
    },
    '栅格模板': {
        eName: 'grid-template',
        values: gridRCMap
    },
    间隙: 'gap',
    '栅格行开始': {
        eName: 'grid-row-start',
        values: {
            跨越: 'span'
        }
    },
    '栅格行结束': {
        eName: 'grid-row-end',
        values: {
            跨越: 'span'
        }
    },
    '栅格行': {
        eName: 'grid-row',
        values: {
            跨越: 'span'
        }
    },
    '栅格列开始': {
        eName: 'grid-column-start',
        values: {
            跨越: 'span'
        }
    },
    '栅格列结束': {
        eName: 'grid-column-end',
        values: {
            跨越: 'span'
        }
    },
    '栅格列': {
        eName: 'grid-column',
        values: {
            跨越: 'span'
        }
    },
    '栅格区域': {
        eName: 'grid-area',
        values: {
            跨越: 'span'
        }
    },
    '栅格自动流': {
        eName: 'grid-auto-flow',
        values: {
            行: 'row',
            列: 'column',
            密集: 'dense'
        }
    },
    '栅格自动列': {
        eName: 'grid-auto-columns',
        values: gridRCMap
    },
    '栅格自动行': {
        eName: 'grid-auto-rows',
        values: gridRCMap
    },
    连字符: {
        eName: 'hyphens',
        values: {
            手动: 'manual'
        }
    },
    字符间距: 'letter-spacing',
    分行: {
        eName: 'line-break',
        values: {
            宽松: 'loose',
            严格: 'strict',
            任意: 'anywhere',
        }
    },
    对象适应: {
        eName: 'object-fit',
        values: {
            包含: 'contain',
            覆盖: 'cover',
            填充: 'fill',
            比例缩小: 'scale-down'
        }
    },
    对象位置: {
        eName: 'object-position',
        values: {
            左: 'left',
            右: 'right',
            居中: 'center',
            上: 'top',
            下: 'bottom'
        }
    },
    孤行数: 'orphans',
    滚动行为: {
        eName: 'scroll-behavior',
        values: {
            平滑: 'smooth'
        }
    },
    tab大小: 'tab-size',
    用户选取: {
        eName: 'user-select',
        values: {
            全部: 'all',
            文本: 'text',
            限制: 'contain',
        }
    },
    断字: {
        eName: 'word-break',
        values: {
            断开全部: 'break-all',
            保留全部: 'keep-all'
        }
    },
    单词间距: 'word-spacing',
    书写模式: {
        eName: 'writing-mode',
        values: {
            '水平-上下': 'horizontal-tb',
            '垂直-左右': 'vertical-lr',
            '垂直-右左': 'vertical-rl'
        }
    },
    z轴索引: 'z-index',
    旋转: 'rotate',
    文本下划线偏移: 'text-underline-offset',
    '统一码-双向': {
        eName: 'unicode-bidi',
        values: {
            '嵌入': 'embed',
            '双向重载': 'bidi-override',
            '隔离': 'isolate',
            '隔离重载': 'isolate-override',
            '纯文本': 'plaintext'
        }
    },
}

const shorthandMap = {
    动画: {
        eName: 'animation',
        values: {
            ...propertyMap.动画方向.values,
            ...propertyMap.动画填充模式.values,
            ...propertyMap.动画重复次数.values,
            ...propertyMap.动画播放状态.values,
            ...propertyMap.动画时序函数.values
        }
    },
    背景: {
        eName: 'background',
        values: {
            ...propertyMap['背景附着'].values,
            ...propertyMap['背景裁剪'].values,
            ...propertyMap['背景颜色'].values,
            ...propertyMap['背景图像'].values,
            ...propertyMap['背景位置'].values,
            ...propertyMap['背景平铺'].values,
            ...propertyMap['背景大小'].values
        }
    },
    '边框块': {
        eName: 'border-block',
        values: {
            ...propertyMap['边框块颜色'].values,
            ...propertyMap['边框块样式'].values,
            ...propertyMap['边框块宽度'].values
        }
    },
    '边框块结束': {
        eName: 'border-block-end',
        values: {
            ...propertyMap['边框块结束颜色'].values,
            ...propertyMap['边框块结束样式'].values,
            ...propertyMap['边框块结束宽度'].values
        }
    },
    '边框块开始': {
        eName: 'border-block-start',
        values: {
            ...propertyMap['边框块开始颜色'].values,
            ...propertyMap['边框块开始样式'].values,
            ...propertyMap['边框块开始宽度'].values
        }
    },
    '边框-下': {
        eName: 'border-bottom',
        values: {
            ...propertyMap['边框-下-颜色'].values,
            ...propertyMap['边框-下-样式'].values,
            ...propertyMap['边框-下-宽度'].values
        }
    },
    '边框-上': {
        eName: 'border-top',
        values: {
            ...propertyMap['边框-上-颜色'].values,
            ...propertyMap['边框-上-样式'].values,
            ...propertyMap['边框-上-宽度'].values
        }
    },
    '边框-左': {
        eName: 'border-left',
        values: {
            ...propertyMap['边框-左-颜色'].values,
            ...propertyMap['边框-左-样式'].values,
            ...propertyMap['边框-左-宽度'].values
        }
    },
    '边框-右': {
        eName: 'border-right',
        values: {
            ...propertyMap['边框-右-颜色'].values,
            ...propertyMap['边框-右-样式'].values,
            ...propertyMap['边框-右-宽度'].values
        }
    },
    '边框颜色': {
        eName: 'border-color',
        values: {
            ...propertyMap['边框-上-颜色'].values,
            ...propertyMap['边框-下-颜色'].values,
            ...propertyMap['边框-左-颜色'].values,
            ...propertyMap['边框-右-颜色'].values
        }
    },
    '边框样式': {
        eName: 'border-style',
        values: {
            ...propertyMap['边框-上-样式'].values,
            ...propertyMap['边框-下-样式'].values,
            ...propertyMap['边框-左-样式'].values,
            ...propertyMap['边框-右-样式'].values
        }
    },
    '边框宽度': {
        eName: 'border-width',
        values: {
            ...propertyMap['边框-上-宽度'].values,
            ...propertyMap['边框-下-宽度'].values,
            ...propertyMap['边框-左-宽度'].values,
            ...propertyMap['边框-右-宽度'].values
        }
    },
    '边框': {
        eName: 'border',
        values: {
            ...propertyMap['边框-上-颜色'].values,
            ...propertyMap['边框-下-颜色'].values,
            ...propertyMap['边框-左-颜色'].values,
            ...propertyMap['边框-右-颜色'].values,
            ...propertyMap['边框-上-样式'].values,
            ...propertyMap['边框-下-样式'].values,
            ...propertyMap['边框-左-样式'].values,
            ...propertyMap['边框-右-样式'].values,
            ...propertyMap['边框-上-宽度'].values,
            ...propertyMap['边框-下-宽度'].values,
            ...propertyMap['边框-左-宽度'].values,
            ...propertyMap['边框-右-宽度'].values
        }
    },
    '边框图像': {
        eName: 'border-image',
        values: {
            ...propertyMap['边框图像平铺'].values,
            ...propertyMap['边框图像内移量'].values,
            ...propertyMap['边框图像来源'].values
        }
    },
    '边框行内': {
        eName: 'border-inline',
        values: {
            ...propertyMap['边框行内颜色'].values,
            ...propertyMap['边框行内样式'].values,
            ...propertyMap['边框行内宽度'].values
        }
    },
    '边框行内结束': {
        eName: 'border-inline-end',
        values: {
            ...propertyMap['边框行内结束颜色'].values,
            ...propertyMap['边框行内结束样式'].values,
            ...propertyMap['边框行内结束宽度'].values
        }
    },
    '边框行内开始': {
        eName: 'border-inline-start',
        values: {
            ...propertyMap['边框行内开始颜色'].values,
            ...propertyMap['边框行内开始样式'].values,
            ...propertyMap['边框行内开始宽度'].values
        }
    },
    列规则: {
        eName: 'column-rule',
        values: {
            ...propertyMap['列规则-颜色'].values,
            ...propertyMap['列规则-样式'].values,
            ...propertyMap['列规则-宽度'].values
        }
    },
    列表样式: {
        eName: 'list-style',
        values: {
            ...propertyMap['列表样式-图像'].values,
            ...propertyMap['列表样式-位置'].values,
            ...propertyMap['列表样式-类型'].values
        }
    },
    文本修饰: {
        eName: 'text-decoration',
        values: {
            ...propertyMap['文本修饰-线'].values,
            ...propertyMap['文本修饰-颜色'].values,
            ...propertyMap['文本修饰-样式'].values,
            ...propertyMap['文本修饰-粗细'].values,
        }
    },
    过渡: {
        eName: 'transition',
        values: {
            ...propertyMap['过渡属性'].values,
            ...propertyMap['过渡时序函数'].values
        }
    },
    轮廓: {
        eName: 'outline',
        values: {
            ...propertyMap['轮廓颜色'].values,
            ...propertyMap['轮廓样式'].values,
            ...propertyMap['轮廓宽度'].values
        }
    },
    字体: {
        eName: 'font',
        values: {
            ...propertyMap['字体大小'].values,
            ...propertyMap['字体样式'].values,
            ...propertyMap['字体粗细'].values
        }
    },
    弹性盒: {
        eName: 'flex',
        values: {
            ...propertyMap['弹性盒基准'].values
        }
    },
    弹性盒流: {
        eName: 'flex-flow',
        values: {
            ...propertyMap['弹性盒方向'].values,
            ...propertyMap['弹性盒换行'].values
        }
    },
    栅格: {
        eName: 'grid',
        values: {
            ...propertyMap['栅格自动列'].values,
            ...propertyMap['栅格自动流'].values
        }
    },
    内容背景混合模式: {
        eName: 'mix-blend-mode',
        values: propertyMap['背景混合模式'].values
    }
}

const identMap = {
    且: 'and',
    非: 'not',
    初始: 'initial',
    继承: 'inherit',
    未设置: 'unset',
    自动: 'auto',
    无: 'none',
    正常: 'normal',
    计算: 'calc',
    计数: 'counters'
}

const dimensionMap = {
    度: 'deg',
    梯度: 'grad',
    弧度: 'rad',
    转: 'turn',
    像素: 'px'
}

module.exports = {
    atruleMap,
    pseudoElementMap,
    pseudoClassMap,
    propertyMap,
    shorthandMap,
    identMap,
    dimensionMap,
    mediaQueryMap
}