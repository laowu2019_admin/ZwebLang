const utils = require("../utils");
const posthtml = require('posthtml');
const zhtmtest = require('./zhtmtest');
const prettier = require("prettier");

function zhtmTranspile(htmlFile, zhtmFileContent) {
    const htmlFileContent = transpile(zhtmFileContent);
    utils.writeFileContents(htmlFile, prettier.format(htmlFileContent, {parser : 'html'}));
}

function transpile(source) {
    const res = posthtml()
    .use(zhtmtest.test())
    .process(source, { sync: true })
    .html;
    
    const result = posthtml()
    .use(zhtmtest.secondPass())
    .process(res, {sync: true})
    .html

    return result;
}


module.exports = {
    zhtmTranspile,
    transpile
}