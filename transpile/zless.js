const utils = require("../utils");
const csstree = require("css-tree");
const less = require("less");
const prettier = require("prettier");
const zcss = require("./zcss");

// function zlessTranspile(lessFile, zlessFileContent) {
//   const lessFileContent = transpile(zlessFileContent);
//   console.log(lessFileContent);
//   utils.writeFileContents(lessFile, lessFileContent);
// }

function zlessTranspile(lessFile, zlessFileContent) {
  // parse zless to zcss
  less.render(zlessFileContent)
    .then (function(output) {
        // parse CSS to AST
        var ast = csstree.parse(output.css);
        // traverse AST and modify it
        zcss.astHandler(ast);
    
        // generate CSS from AST
        const result = prettier.format(csstree.generate(ast), { parser: "css" });
        utils.writeFileContents(lessFile, result);
    });
  
}

module.exports = {
  zlessTranspile,
};
