const relMap = {
    eName: 'rel',
    attrValues: {
        备选: 'alternate',
        作者: 'author',
        书签: 'bookmark',
        规范: 'canonical',
        dns预取: 'dns-prefetch',
        外部: 'external',
        帮助: 'help',
        图标: 'icon',
        许可: 'license',
        清单: 'manifest',
        模块预加载: 'modulepreload',
        下一个: 'next',
        不跟随: 'nofollow',
        无打开者: 'noopener',
        无访问来源: 'noreferrer',
        回ping: 'pingback',
        预取: 'prefetch',
        预连接: 'preconnect',
        预渲染: 'prerender',
        预加载: 'preload',
        上一个: 'prev',
        搜索: 'search',
        短链接: 'shortlink',
        样式表: 'stylesheet',
        标签: 'tag'
    }
}

const targetMap = {
    eName: 'target',
    attrValues: {
        _自身: '_self',
        _空白: '_blank',
        _父: '_parent',
        _顶层: '_top'
    }
}

const crossMap = {
    eName: 'crossorigin',
    attrValues: {
        匿名: 'anonymous',
        使用凭证: 'use-credentials'
    }
}

const preloadMap = {
    eName: 'preload',
    attrValues: {
        否: 'none',
        元数据: 'metadata',
        自动: 'auto'
    }
}

const metaContentMap = {  // 待完善
    eName: 'content',
    attrValues: {
        无访问来源: 'no-referrer', // 用于 referrer
        来源域: 'origin',
        不安全url: 'unsafe-url',
        正常: 'normal', // 用于 color-scheme
        仅浅色: 'only light'
    }
}

const metaViewportContentMap = {  // 待完善
    keys: {
        宽度: 'width',
        高度: 'height',
        初始比例: 'initial-scale',
        最大比例: 'maximum-scale',
        最小比例: 'minimum-scale',
        用户可缩放: 'user-scalable',
        适合视口: 'viewport-fit',
    },
    vals: {
        设备宽度: 'device-width',
        设备高度: 'device-height',
        是: 'yes',
        否: 'no',
        自动: 'auto',
        包含: 'contain',
        覆盖: 'cover'
    }
}

const referrerMap = {  // iframe 待完成
    eName: 'referrerpolicy',
    attrValues: {
        无访问来源: 'no-referrer',
        降级时无访问来源: 'no-referrer-when-downgrade',
        来源域: 'origin',
        跨域时来源域: 'origin-when-cross-origin',
        同源: 'same-origin',
        严格来源域: 'strict-origin',
        跨域时严格来源域: 'strict-origin-when-cross-origin',
        不安全url: 'unsafe-url'
    }
}

const sandboxMap = {  // iframe 待完成
    eName: 'sandbox',
    attrValues: {
        允许下载: 'allow-downloads',
        允许表单: 'allow-forms',
        允许模态窗口: 'allow-modals',
        允许弹出窗口: 'allow-popups',
        允许同源: 'allow-same-origin',
        允许脚本: 'allow-scripts',
    }
}

const autocompleteMap = {
    eName: 'autocomplete',
    attrValues: {
        关: 'off',
        开: 'on',
        姓名: 'name',
        电邮: 'email',
        用户名: 'username',
        新密码: 'new-password',
        现密码: 'current-password',
        一次性码: 'one-time-code',
        职位: 'organization-title',
        组织: 'organization',
        街道地址: 'street-address',
        地址行1: 'address-line1',
        地址行2: 'address-line2',
        地址行3: 'address-line3',
        地址级别4: 'address-level4',
        地址级别3: 'address-level3',
        地址级别2: 'address-level2',
        地址级别1: 'address-level1',
        国家地区代码: 'country',
        国家地区名称: 'country-name',
        邮政编码: 'postal-code',
        '支付工具-姓名': 'cc-name',
        '支付工具-号码': 'cc-number',
        '支付工具-到期日': 'cc-exp',
        '支付工具-到期月': 'cc-exp-month',
        '支付工具-到期年': 'cc-exp-year',
        '支付工具-安全码': 'cc-csc',
        '支付工具-类型': 'cc-type',
        语言: 'language',
        出生日期: 'bday',
        出生之日: 'bday-day',
        出生之月: 'bday-month',
        出生之年: 'bday-year',
        性别: 'sex',
        电话: 'tel',
        传真: 'fax',
        手机: 'mobile',
        电话分机: 'tel-extension',
        即时消息: 'impp',
        // url
        照片: 'photo'
    }
}

const tagMap = {
    // 主根元素
    网页: {
        eName: 'html',
        attrs: {
            语言: 'lang'
        }
    },
    // 文档元数据
    基址: {
        eName: 'base',
        attrs: {
            网址: 'href',
            目标: targetMap
        }
    },
    页头: 'head',
    链接: {
        eName: 'link',
        attrs: {
            作为: {
                eName: 'as',
                attrValues: {
                    音频: 'audio',
                    文档: 'document',
                    嵌入: 'embed',
                    拿取: 'fetch',
                    字体: 'font',
                    图像: 'image',
                    对象: 'object',
                    脚本: 'script',
                    样式: 'style',
                    轨道: 'track',
                    视频: 'video',
                    工人: 'worker'
                }
            },
            跨域: crossMap,
            禁用: 'disabled',
            网址: 'href',
            网址语言: 'hreflang',
            图像尺寸: 'imagesizes',
            图像源集: 'imagesrcset',
            媒体: 'media',
            关系: relMap,
            尺寸: {
                eName: 'sizes',
                attrValues: {
                    任意: 'any'
                }
            },
            提示: 'title',
            类型: 'type',
        }
    },
    元数据: {
        eName: 'meta',
        attrs: {
            字符集: 'charset',
            内容: metaContentMap,
            http等效串: 'http-equiv',
            名称: {
                eName: 'name',
                attrValues: { // 有待补充
                    应用名称: 'application-name',
                    作者: 'author',
                    描述: 'description',
                    生成器: 'generator',
                    关键词: 'keywords',
                    访问来源: 'referrer',
                    主题颜色: 'theme-color',
                    颜色方案: 'color-scheme',
                    视口: 'viewport'
                }
            }
        }
    },
    样式: {
        eName: 'style',
        attrs: {
            类型: 'type',
            媒体: 'media',
            一次数: 'nonce',
            标题: 'title',
            语言: 'lang', // for vue
            作用域: 'scoped' // for vue
        }
    },
    抬头: 'title',
    // 分区根元素
    主体: {
        eName: 'body',
        attrs: {
            打印后: 'onafterprint',
            打印前: 'onbeforeprint',
            卸载前: 'onbeforeunload',
            井号片段改变时: 'onhashchange',
            收到消息时: 'onmessage',
            离线时: 'onoffline',
            上线时: 'ononline',
            弹出状态时: 'onpopstate',
            恢复时: 'onredo',
            存储时: 'onstorage',
            撤消时: 'onundo',
            卸载时: 'onunload'
        }
    },
    // 内容分区
    地址: 'address',
    内容区域: 'article',
    附加区域: 'aside',
    尾部: 'footer',
    头部: 'header',
    标题1: 'h1',
    标题2: 'h2',
    标题3: 'h3',
    标题4: 'h4',
    标题5: 'h5',
    标题6: 'h6',
    主要: 'main',
    导航: 'nav',
    区块: 'section',
    // 文本内容
    长引文: {
        eName: 'blockquote',
        attrs: {
            引用: 'cite'
        }
    },
    描述: 'dl',
    术语: 'dt',
    详解: 'dd',
    块: 'div',
    图形标题: 'figcaption',
    图形: 'figure',
    水平线: 'hr',
    无序: 'ul',
    有序: {
        eName: 'ol',
        attrs: {
            倒序: 'reversed',
            起始: 'start',
            类型: 'type'
        }
    },
    条目: {
        eName: 'li',
        attrs: {
            值: 'value'
        }
    },
    段: 'p',
    原样: 'pre',
    // 内联文本语义
    超链接: {
        eName: 'a',
        attrs: {
            下载: 'download',
            网址: 'href',
            网址语言: 'hreflang',
            // ping
            关系: relMap,
            目标: targetMap,
            类型: 'type'
        }
    },
    缩写: 'abbr',
    粗体: 'b',
    双向隔离: 'bdi',
    双向重载: 'bdo',
    换行: 'br',
    引用: 'cite',
    代码: 'code',
    数据: {
        eName: 'data',
        attrs: {
            值: 'value'
        }
    },
    定义: 'dfn',
    强调: 'em',
    斜体: 'i',
    键盘: 'kbd',
    突出: 'mark',
    短引文: {
        eName: 'q',
        attrs: {
            引用: 'cite'
        }
    },
    注音文本: 'rb',
    拼音: 'rt',
    注音备选: 'rp',
    注音语义: 'rtc',
    注音: 'ruby',
    删除线: 's',
    样本: 'samp',
    小字: 'small',
    范围: 'span',
    重要: 'strong',
    下标: 'sub',
    上标: 'sup',
    时间: {
        eName: 'time',
        attrs: {
            日期时间: 'datetime'
        }
    },
    下划线: 'u',
    变量: 'var',
    软换行: 'wbr',
    // 图片和多媒体
    区域: { // 仅用于 地图 元素内部
        eName: 'area',
        attrs: {
            替代文本: 'alt',
            坐标: 'coords',
            下载: 'download',
            网址: 'href',
            网址语言: 'hreflang',
            // ping
            关系: relMap,
            形状: {
                eName: 'shape',
                attrValues: {
                    长方形: 'rect',
                    圆形: 'circle',
                    多边形: 'poly',
                    默认: 'default'
                }
            },
            目标: targetMap
        }
    },
    音频: {
        eName: 'audio',
        attrs: {
            自动播放: 'autoplay',
            控件: 'controls',
            跨域: crossMap,
            循环: 'loop',
            静音: 'muted',
            预加载: preloadMap,
            源: 'src'
        }
    },
    图像: {
        eName: 'img',
        attrs: {
            替代文本: 'alt',
            跨域: crossMap,
            解码: {
                eName: 'decoding',
                attrValues: {
                    同步: 'sync',
                    异步: 'async',
                    自动: 'auto'
                }
            },
            加载: { // 实验属性
                eName: 'loading',
                attrValues: {
                    立即: 'eager',
                    惰性: 'lazy'
                }
            },
            高度: 'height',
            是地图: 'ismap',
            访问来源策略: referrerMap,
            尺寸: 'sizes', // 其值需进一步中文化
            源: 'src',
            源集: 'srcset',
            宽度: 'width',
            使用地图: 'usemap'
        }
    },
    地图: {
        eName: 'map',
        attrs: {
            名称: 'name'
        }
    },
    轨道: {
        eName: 'track',
        attrs: {
            默认: 'default',
            种类: {
                eName: 'kind',
                attrValues: {
                    字幕: 'subtitles',
                    解说词: 'captions',
                    说明: 'descriptions',
                    章节: 'chapters',
                    元数据: 'metadata'
                }
            },
            标签: 'label',
            源: 'src',
            源语言: 'srclang'
        }
    },
    视频: {
        eName: 'video',
        attrs: {
            自动播放: 'autoplay',
            缓冲内容: 'buffered',
            控件: 'controls',
            跨域: crossMap,
            高度: 'height',
            循环: 'loop',
            静音: 'muted',
            内联播放: 'playsinline',
            海报: 'poster',
            预加载: preloadMap,
            源: 'src',
            宽度: 'width'
        }
    },
    // 内嵌内容
    嵌入: {
        eName: 'embed',
        attrs: {
            高度: 'height',
            源: 'src',
            类型: 'type',
            宽度: 'width'
        }
    },
    内框: {
        eName: 'iframe',
        attrs: {
            允许: {  // 有待研究
                eName: 'allow',
                attrValues: {
                    全屏: 'fullscreen',
                    付款: 'payment'
                }
            },
            高度: 'height',
            加载: { // 实验属性
                eName: 'loading',
                attrValues: {
                    立即: 'eager',
                    惰性: 'lazy'
                }
            },
            名称: 'name',
            访问来源策略: referrerMap,
            沙箱: sandboxMap,
            源: 'src',
            源文档: 'srcdoc',
            宽度: 'width'
        }
    },
    对象: {
        eName: 'object',
        attrs: {
            数据: 'data',
            表单: 'form',
            高度: 'height',
            名称: 'name',
            类型: 'type',
            使用地图: 'usemap',
            宽度: 'width'
        }
    },
    参数: {
        eName: 'param',
        attrs: {
            名称: 'name',
            值: 'value'
        }
    },
    图片: 'picture',
    传送门: {
        eName: 'portal',
        attrs: {
            访问来源策略: referrerMap,
            源: 'src'
        }
    },
    来源: {
        eName: 'source',
        attrs: {
            媒体: 'media',
            尺寸: 'sizes',
            源: 'src',
            源集: 'srcset',
            类型: 'type'
        }
    },
    // SVG 和 MathML
    矢量图: {  // 待细化
        eName: 'SVG',
        attrs: {
            高度: 'height',
            保留纵横比: 'preserveAspectRatio',
            视盒: 'viewBox',
            宽度: 'width'
            // x, y
        }
    },
    数学: {  // 待细化
        eName: 'math',
        attrs: {
            方向: 'dir',
            网址: 'href',
            背景颜色: 'mathbackground',
            文本颜色: 'mathcolor',
            显示: {
                eName: 'display',
                attrValues: {
                    块级: 'block',
                    行内: 'inline'
                }
            }
        }
    },
    // 脚本
    画布: {  // 待细化
        eName: 'canvas',
        attrs: {
            高度: 'height',
            宽度: 'width'
        }
    },
    代脚本: 'noscript',
    脚本: {  // 待细化
        eName: 'script',
        attrs: {
            异步: 'async',
            跨域: crossMap,
            延迟: 'defer',
            完整性: 'integrity',
            非模块: 'nomodule',
            一次数: 'nonce',
            访问来源策略: referrerMap,
            源: 'src',
            类型: {
                eName: 'type',
                attrValues: {
                    模块: 'module'
                }
            }
        }
    },
    // 编辑标识
    删除: {
        eName: 'del',
        attrs: {
            引用: 'cite',
            日期时间: 'datetime'
        }
    },
    插入: {
        eName: 'ins',
        attrs: {
            引用: 'cite',
            日期时间: 'datetime'
        }
    },
    // 表格内容
    表格标题: 'caption',
    列: {
        eName: 'col',
        attrs: {
            跨列: 'span'
        }
    },
    列组: {
        eName: 'colgroup',
        attrs: {
            跨列: 'span'
        }
    },
    表格: 'table',
    表体: 'tbody',
    单元格: {
        eName: 'td',
        attrs: {
            跨列: 'colspan',
            头id: 'headers',
            跨行: 'rowspan'
        }
    },
    表尾: 'tfoot',
    表头: 'thead',
    头单元格: {
        eName: 'th',
        attrs: {
            缩写: 'abbr',
            跨列: 'colspan',
            头id: 'headers',
            跨行: 'rowspan',
            范围: {
                eName: 'scope',
                attrValues: {
                    行: 'row',
                    列: 'col',
                    行组: 'rowgroup',
                    列组: 'colgroup'
                }
            }
        }
    },
    行: 'tr',
    // 表单
    按钮: {
        eName: 'button',
        attrs: {
            自动获得焦点: 'autofocus',
            禁用: 'disabled',
            表单: 'form',
            表单操作: 'formaction',
            表单编码类型: {
                eName: 'formenctype',
                attrValues: {
                    '应用程序/x-www-表单-url编码': 'application/x-www-form-urlencoded',
                    '多部分/表单-数据': 'multipart/form-data',
                    '文本/普通': 'text/plain'
                }
            },
            表单方法: { // https://www.cnblogs.com/xinglongbing521/p/10281860.html
                eName: 'formmethod',
                attrValues: {
                    创建: 'post',
                    查看: 'get'
                }
            },
            表单不验证: 'formnovalidate',
            表单目标: {
                eName: 'formtarget',
                attrValues: {
                    _自身: '_self',
                    _空白: '_blank',
                    _父: '_parent',
                    _顶层: '_top'
                }
            },
            名称: 'name',
            类型: {
                eName: 'type',
                attrValues: {
                    提交: 'submit',
                    重置: 'reset',
                    按钮: 'button'
                }
            },
            值: 'value'
        }
    },
    数据列表: 'datalist',
    控件组: {
        eName: 'fieldset',
        attrs: {
            禁用: 'disabled',
            表单: 'form',
            名称: 'name'
        }
    },
    表单: {
        eName: 'form',
        attrs: {
            接受字符集: 'accept-charset',
            自动补全: {
                eName: 'autocomplete',
                attrValues: {
                    关: 'off',
                    开: 'on'
                }
            },
            名称: 'name',
            关系: relMap,
            操作: 'action',
            编码类型: {
                eName: 'enctype',
                attrValues: {
                    '应用程序/x-www-表单-url编码': 'application/x-www-form-urlencoded',
                    '多部分/表单-数据': 'multipart/form-data',
                    '文本/普通': 'text/plain'
                }
            },
            方法: { // https://www.cnblogs.com/xinglongbing521/p/10281860.html
                eName: 'method',
                attrValues: {
                    创建: 'post',
                    查看: 'get',
                    对话框: 'dialog'
                }
            },
            不验证: 'novalidate',
            目标: targetMap
        }
    },
    输入: {
        eName: 'input',
        attrs: {
            接受: 'accept',
            替代文本: 'alt',
            自动补全: autocompleteMap,
            自动获得焦点: 'autofocus',
            捕捉: {
                eName: 'capture',
                attrValues: {
                    用户: 'user',
                    环境: 'environment'
                }
            },
            勾选: 'checked',
            方向名称: 'dirname',
            禁用: 'disabled',
            表单: 'form',
            表单操作: 'formaction',
            表单编码类型: {
                eName: 'formenctype',
                attrValues: {
                    '应用程序/x-www-表单-url编码': 'application/x-www-form-urlencoded',
                    '多部分/表单-数据': 'multipart/form-data',
                    '文本/普通': 'text/plain'
                }
            },
            表单方法: { // https://www.cnblogs.com/xinglongbing521/p/10281860.html
                eName: 'formmethod',
                attrValues: {
                    创建: 'post',
                    查看: 'get',
                    对话框: 'dialog'
                }
            },
            表单不验证: 'formnovalidate',
            表单目标: {
                eName: 'formtarget',
                attrValues: {
                    _自身: '_self',
                    _空白: '_blank',
                    _父: '_parent',
                    _顶层: '_top'
                }
            },
            高度: 'height',
            // id
            输入模式: {
                eName: 'inputmode',
                attrValues: {
                    无: 'none',
                    文本: 'text',
                    电话: 'tel',
                    // url
                    电邮: 'email',
                    数字: 'numeric',
                    小数: 'decimal',
                    搜索: 'search'
                }
            },
            列表: 'list',
            最大值: 'max',
            最大长度: 'maxlength',
            最小值: 'min',
            最小长度: 'minlength',
            多选: 'multiple',
            名称: 'name',
            模式: 'pattern',
            占位符: 'placeholder',
            只能读: 'readonly',
            必填: 'required',
            大小: 'size',
            源: 'src',
            步长: {
                eName: 'step',
                attrValues: {
                    任意: 'any'
                }
            },
            tab索引: 'tabindex',
            提示: 'title',
            类型: {
                eName: 'type',
                attrValues: {
                    按钮: 'button',
                    复选框: 'checkbox',
                    颜色: 'color',
                    日期: 'date',
                    '日期时间-本地': 'datetime-local',
                    电邮: 'email',
                    文件: 'file',
                    隐藏: 'hidden',
                    图像: 'image',
                    月份: 'month',
                    数字: 'number',
                    密码: 'password',
                    单选按钮: 'radio',
                    范围: 'range',
                    重置: 'reset',
                    搜索: 'search',
                    提交: 'submit',
                    电话: 'tel',
                    文本: 'text',
                    时间: 'time',
                    // url
                    周: 'week'
                }
            },
            值: 'value',
            宽度: 'width',
            真值: 'true-value', // vue v-model 复选框 相关
            假值: 'false-value'
        }
    },
    标签: {
        eName: 'label',
        attrs: {
            用于: 'for'
        }
    },
    组标题: 'legend',
    计量表: {
        eName: 'meter',
        attrs: {
            值: 'value',
            最小值: 'min',
            最大值: 'max',
            低值: 'low',
            高值: 'high',
            最优值: 'optimum',
            表单: 'form'
        }
    },
    选项组: {
        eName: 'optgroup',
        attrs: {
            禁用: 'disabled',
            标签: 'label'
        }
    },
    选项: {
        eName: 'option',
        attrs: {
            禁用: 'disabled',
            标签: 'label',
            选中: 'selected',
            值: 'value'
        }
    },
    输出: {
        eName: 'output',
        attrs: {
            用于: 'for',
            表单: 'form',
            名称: 'name'
        }
    },
    进度条: {
        eName: 'progress',
        attrs: {
            值: 'value',
            最大值: 'max'
        }
    },
    选择框: {
        eName: 'select',
        attrs: {
            自动补全: autocompleteMap,
            自动获得焦点: 'autofocus',
            禁用: 'disabled',
            表单: 'form',
            多选: 'multiple',
            名称: 'name',
            必填: 'required',
            大小: 'size'
        }
    },
    文本域: {
        eName: 'textarea',
        attrs: {
            自动补全: {
                eName: 'autocomplete',
                attrValues: {
                    关: 'off',
                    开: 'on'
                }
            },
            自动获得焦点: 'autofocus',
            列数: 'cols',
            禁用: 'disabled',
            表单: 'form',
            最大长度: 'maxlength',
            最小长度: 'minlength',
            名称: 'name',
            占位符: 'placeholder',
            只能读: 'readonly',
            必填: 'required',
            行数: 'rows',
            拼写检查: {
                eName: 'spellcheck',
                attrValues: {
                    真: 'true',
                    默认: 'default',
                    假: 'false'
                }
            },
            换行: {
                eName: 'wrap',
                attrValues: {
                    硬: 'hard',
                    软: 'soft',
                    关: 'off'
                }
            }
        }
    },
    // 交互元素
    详情: {
        eName: 'details',
        attrs: {
            打开: 'open'
        }
    },
    对话框: {
        eName: 'dialog',
        attrs: {
            打开: 'open'
        }
    },
    菜单: {
        eName: 'menu',
        attrs: {
            类型: {
                eName: 'type',
                attrValues: {
                    工具栏: 'toobar'
                }
            }
        }
    },
    摘要: 'summary',
    // Web 组件
    插槽: {
        eName: 'slot',
        attrs: {
            名称: 'name'
        }
    },
    模板: 'template',
    // vue 内置组件
    组件: {
        eName: 'component',
        attrs: {
            用作: 'is',
            内联模板: 'inline-template'
        }
    },
    过渡: {
        eName: 'transition',
        attrs: {
            名称: 'name',
            类型: 'type' // 待补充, 还有很多属性和事件
        }
    },
    过渡组: {
        eName: 'transition-group',
        attrs: {
            名称: 'name',
            类型: 'type' // 待补充, 还有很多属性和事件
        }
    },
    保持活动: {
        eName: 'keep-alive',
        attrs: {
            包括: 'include',
            排除: 'exclude',
            最多: 'max'
        }
    },
}

const globalAttrMap = {
    快捷键: 'accessKey',
    自动大写: {
        eName: 'autocapitalize',
        attrValues: {
            关: 'off',
            开: 'on',
            单词: 'words',
            字母: 'characters'
        }
    },
    类: 'class',
    内容可编辑: {
        eName: 'contenteditable',
        attrValues: {
            真: 'true',
            假: 'false'
        }
    },
    // 数据-*: 'data-*',
    方向: {
        eName: 'dir',
        attrValues: {
            左至右: 'ltr',
            右至左: 'rtl',
            自动: 'auto'
        }
    },
    可拖动: {
        eName: 'draggable',
        attrValues: {
            真: 'true',
            假: 'false'
        }
    },
    回车键呈现: 'enterkeyhint',
    隐藏: 'hidden',
    输入模式: 'inputmode',
    用作: 'is',
    // 元素*: 'item*',
    语言: 'lang',
    一次数: 'nonce',
    部件: 'part', // shadow tree?
    插槽: 'slot', // shadow tree?
    拼写检查: {
        eName: 'spellcheck',
        attrValues: {
            真: 'true',
            假: 'false'
        }
    },
    样式: 'style',
    tab索引: 'tabindex',
    提示: 'title',
    翻译: {
        eName: 'translate',
        attrValues: {
            是: 'yes',
            否: 'no'
        }
    },
    // vue 属性
    键: 'key',
    引用: 'ref',
    // vue 指令
    'v-文本' : 'v-text',
    // 'v-html' : 'v-html',
    'v-显示' : 'v-show',
    'v-如果' : 'v-if',
    'v-或如' : 'v-else-if',
    'v-否则' : 'v-else',
    'v-遍历' : 'v-for', // 迭代器?
    // 'v-取' : 'v-for',
    'v-事件' : 'v-on',   // @
    // 'v-绑定' : 'v-bind', // :
    'v-双绑' : 'v-model',
    'v-双绑.惰性' : 'v-model.lazy',
    'v-双绑.数字' : 'v-model.number',
    'v-双绑.修剪' : 'v-model.trim',
    'v-插槽' : 'v-slot', // #
    'v-原样' : 'v-pre',
    'v-遮盖' : 'v-cloak',
    'v-一次' : 'v-once'
}

const eventMap = {
    中止时: 'onabort',
    自动补全时: 'onautocomplete',
    自动补全出错时: 'onautocompleteerror',
    失去焦点时: 'onblur',
    取消时: 'oncancel',
    可播放时: 'oncanplay',
    可播放到底时: 'oncanplaythrough',
    改变时: 'onchange',
    点击时: 'onclick',
    关闭时: 'onclose',
    右击鼠标时: 'oncontextmenu',
    信号改变时: 'oncuechange', /* 配合 轨道 元素使用 */
    双击时: 'ondblclick',
    拖动时: 'ondrag',
    拖动结束时: 'ondragend',
    拖动进入时: 'ondragenter',
    拖动退出时: 'ondragexit',
    拖动离开时: 'ondragleave',
    拖动经过时: 'ondragover',
    拖动开始时: 'ondragstart',
    释放时: 'ondrop',
    时长改变时: 'ondurationchange',
    变空时: 'onemptied',
    结束时: 'onended',
    出错时: 'onerror',
    获得焦点时: 'onfocus',
    输入时: 'oninput',
    无效时: 'oninvalid',
    键按下时: 'onkeydown',
    按键时: 'onkeypress',
    键释放时: 'onkeyup',
    加载完毕时: 'onload',
    数据加载完毕时: 'onloadeddata',
    元数据加载完毕时: 'onloadedmetadata',
    加载开始时: 'onloadstart',
    鼠标按下时: 'onmousedown',
    鼠标进入时: 'onmouseenter',
    鼠标离开时: 'onmouseleave',
    鼠标移动时: 'onmousemove',
    鼠标移出时: 'onmouseout',
    鼠标经过时: 'onmouseover',
    鼠标释放时: 'onmouseup',
    鼠标滚轮滚动时: 'onmousewheel',
    暂停时: 'onpause',
    播放时: 'onplay',
    进行播放时: 'onplaying', /* 待定 */
    下载进行时: 'onprogress',
    速度改变时: 'onratechange',
    重置时: 'onreset',
    调整大小时: 'onresize',
    滚动时: 'onscroll',
    寻位结束时: 'onseeked',
    寻位开始时: 'onseeking',
    选中时: 'onselect',
    显示时: 'onshow',
    排序时: 'onsort',
    停顿时: 'onstalled',
    提交时: 'onsubmit',
    挂起时: 'onsuspend',
    时间更新时: 'ontimeupdate',
    切换时: 'ontoggle',
    音量改变时: 'onvolumechange',
    等待时: 'onwaiting'
}

module.exports = {
    tagMap,
    globalAttrMap,
    eventMap,
    metaViewportContentMap,
    relMap
}