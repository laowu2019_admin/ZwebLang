const bParser = require("@babel/parser");
const generator = require("@babel/generator");
const t = require("@babel/types");
const traverse = require("@babel/traverse").default;
const { comOptsMap } = require("./zvuedata");

function transVue(script) {
    let ast = bParser.parse(script, {sourceType: "unambiguous"});

    traverse(ast, {
        enter(path) {
            if (t.isCallExpression(path.node)) {
                switch (path.node.callee.name) {
                    case '注册组件':
                        let obj = path.node.arguments[0];
                        for (prop of obj.properties) {
                            if (prop.key && prop.key.name) {
                                prop.key.name = comOptsMap[prop.key.name] || prop.key.name;
                            }
                        }
                        path.replaceWith(t.objectExpression(obj.properties))
                        break;
                
                    default:
                        break;
                }
            }
        }
    });

    let options = {
        // concise: true,
        retainLines: true,
        jsescOption: {minimal: true}
    }
    const output = new generator.CodeGenerator(ast, options, script).generate();
    // console.log(output.code);
    return output.code;
}

module.exports = {
    transVue
}