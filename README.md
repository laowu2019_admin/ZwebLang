# 极速中文前端开发语言

#### 介绍
以 VS Code 扩展形式支持使用中文代码进行 Web 开发：创建后缀名为 .z.html/.z.css/.z.less/z.js/z.ts/.v.html 的文件，编写中文代码并保存时，就会生成相应的 .html/.css/.js/.vue 英文代码。

目前为测试版，已完成 html/css/es5/Vue 常用部分的中文化。

![Zweblang](https://www.grasspy.cn/uploads/Zweblang.gif)



#### 使用说明

1.  点击下载[VSC草蟒极速定制绿色便携版](https://www.grasspy.cn/download/VSCodeWinx64Insider1600.rar)。
2.  点击打开或下载[VSC草蟒极速定制绿色便携版使用说明](https://www.grasspy.cn/download/VSC草蟒极速定制绿色便携版使用说明210815.pdf)，按照使用说明开始试用极速中文前端开发语言。注意：示例目录位置有变动，已提升至与 VSC 可执行文件平级的目录中。



#### 参与贡献

1.  参与共同开发和测试
2.  帮助制作中文 Web 开发语言支持文档
3.  帮助校订和增加标签/属性术语，详见 ./transpile/ 目录下的 *data.js 文件
4.  ES 的 .d.ts 文件的中文化
5.  html-data 和 css-data 的中文化, 详见 ./snippets/ 目录下的相应文件

#### 更新说明

+ 0.3.0: 2021-09-13

提供全面的自动补全和智能提示。


