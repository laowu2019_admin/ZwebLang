const path = require('path');
const fs = require('fs');
const mkpath = require("mkpath");

function writeFileContents(filepath, content) {
  return new Promise((resolve, reject) => {
    mkpath(path.dirname(filepath), (err) => {
      if (err) {
        return reject(err);
      }

      fs.writeFile(filepath, content, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(void 0);
        }
      });
    });
  });
}

module.exports = {
  writeFileContents
}